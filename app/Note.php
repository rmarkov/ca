<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    const TYPE_SPECIAL_NEEDS = 'special_needs';

    /**
     * @inheritdoc
     */
    protected $table = 'note';
}
