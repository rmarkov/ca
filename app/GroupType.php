<?php

namespace App;

class GroupType
{
    const TYPE_MEMBERSHIP = 3;
    const TYPE_GUEST = 4;
    const TYPE_ADDON = 6;
}
