<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFavoriteLocation extends Model
{
    /**
     * @inheritdoc
     */
    protected $table = 'user_favorite_location';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function location()
    {
        return $this->hasOne(CourtLocation::class, 'id', 'location_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
