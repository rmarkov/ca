<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    /**
     * @inheritdoc
     */
    protected $table = 'user_group';

    public function group()
    {
        return $this->hasOne(Group::class, 'id', 'group_id');
    }
}
