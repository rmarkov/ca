<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    /**
     * @inheritdoc
     */
    protected $table = 'phone';
}
