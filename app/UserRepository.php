<?php

namespace App;

class UserRepository
{
    /**
     * @var User
     */
    protected $user;

    /**
     * UserRepository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param $userId
     */
    public function getFamilyIds($userId)
    {
        $result[$userId] = (int) $userId;
        $user = $this->user->find($userId);
        if ($user) {
            if ($user->parent_id > 0 && !isset($result[$user->parent_id])) {
                $result[$user->parent_id] = (int) $user->parent_id;
                $result = $this->getFamilyIds($user->parent_id, $result);
            }
            if ($user->primary_id > 0 && !isset($result[$user->primary_id])) {
                $result[$user->primary_id] = (int) $user->primary_id;
                $result = $this->getFamilyIds($user->primary_id, $result);
            }
            $childrenIds = $this->user
                ->select('id')
                ->children($userId)
                ->pluck('id');
            foreach ($childrenIds as $childId) {
                $result[$childId] = (int) $childId;
            }
        }
        return $result;
    }

    /**
     * @param $userId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getFamily($userId)
    {
        $familyIds = $this->getFamilyIds($userId);
        return $this->user->whereIn('id', array_values($familyIds));
    }
}
