<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const CREATED_AT = 'modified_date';
    const UPDATED_AT = 'modified_date';

    /**
     * @inheritdoc
     */
    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $parentId
     * @param $primaryId
     * @return mixed
     */
    public function scopeChildren($query, $userId)
    {
        return $query
            ->where('parent_id', $userId)
            ->orWhere('primary_id', $userId);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userGroups()
    {
        return $this->hasMany(UserGroup::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function favoriteLocations()
    {
        return $this->hasManyThrough(
            CourtLocation::class,
            UserFavoriteLocation::class,
            'user_id',
            'id',
            'id',
            'location_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function phones()
    {
        return $this->hasMany(Phone::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany(Note::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function specialNeedsNotes()
    {
        return $this->notes()->where('type', Note::TYPE_SPECIAL_NEEDS);
    }

}
