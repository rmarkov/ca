<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourtLocation extends Model
{
    /**
     * @inheritdoc
     */
    protected $table = 'court_location';
}
