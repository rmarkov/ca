<?php

use CA\Account\Http\Controllers\V1\AccountProfileController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'api/v1/'], function () {
    Route::get('account-profiles', AccountProfileController::class . '@getAccountProfiles');
});