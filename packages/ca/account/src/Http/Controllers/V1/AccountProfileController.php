<?php

namespace CA\Account\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\User;
use App\UserRepository;
use CA\Account\Http\Resources\AccountProfileResourceCollection;
use Illuminate\Http\Request;

class AccountProfileController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * AccountProfileController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getAccountProfiles(Request $request)
    {
        $familyUsers = $this->userRepository->getFamily($request->userId);
        $familyUsers->with(
            'userGroups.group',
            'favoriteLocations',
            'phones',
            'specialNeedsNotes'
        );
        return new AccountProfileResourceCollection($familyUsers->get());
    }
}
