<?php

namespace CA\Account\Http\Resources;

use App\GroupType;
use Illuminate\Http\Resources\Json\JsonResource;

class UserGroupResource extends JsonResource
{

    protected $groupTypeNames = [
        GroupType::TYPE_MEMBERSHIP => 'membership',
        GroupType::TYPE_GUEST      => 'guestGroup',
        GroupType::TYPE_ADDON      => 'addOn',
    ];

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->group->name,
            'category'  => $this->getCategory(),
        ];
    }

    /**
     * @return string
     */
    protected function getCategory()
    {
        return $this->groupTypeNames[$this->group->group_type];
    }
}
