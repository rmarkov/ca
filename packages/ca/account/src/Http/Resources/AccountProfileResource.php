<?php

namespace CA\Account\Http\Resources;

use App\Http\Resources\NoteResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class AccountProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $phones = $this->getPhones();
        return [
            'userId'     => $this->id,
            'login'      => $this->login,
            'userpic'    => '',
            'email'      => $this->email,
            'firstName'  => $this->first_name,
            'middleName' => $this->middle_name,
            'lastName'   => $this->last_name,
            'address1'   => $this->street_address,
            'address2'   => $this->street_address2,
            'city'       => $this->city,
            'state'      => $this->state,
            'zip'        => $this->zip,
            'accessCardNumber' => '',
            'isPrimary'        => $this->primary_id != 0,
            'isSecondary'      => $this->parent_id == 0,
            'dateOfBirth'      => $this->birth_date,
            'gender'           => $this->sex ? 'Female' : 'Male',
            'cellPhone'        => !empty($phones['cellPhone']) ? $phones['cellPhone'] : '',
            'homePhone'        => !empty($phones['homePhone']) ? $phones['homePhone'] : '',
            'workPhone'        => !empty($phones['workPhone']) ? $phones['workPhone'] : '',
            'otherPhone'       => !empty($phones['otherPhone']) ? $phones['otherPhone'] : '',
            'emergencyPhone'   => !empty($phones['emergencyPhone']) ? $phones['emergencyPhone'] : '',
            'emergencyContact' => '',
            'emergencyRelation' => '',
            'specialNeeds'      => new NoteResourceCollection($this->specialNeedsNotes),
            'favoriteLocations'  => new CourtLocationResourceCollection($this->favoriteLocations),
            'userGroups'        => new UserGroupResourceCollection($this->userGroups),
        ];
    }

    /**
     * @return array
     */
    protected function getPhones()
    {
        $result = [];
        foreach ($this->phones as $phone) {
            switch ($phone->phone_type) {
                case 'Home':
                    $result['homePhone'] = $phone->number;
                case 'Cell':
                    $result['cellPhone'] = $phone->number;
                case 'Work':
                    $result['workPhone'] = $phone->number;
                case 'Emergency':
                    $result['emergencyPhone'] = $phone->number;
                case 'Other':
                    $result['otherPhone'] = $phone->number;
            }
        }
        return $result;
    }
}
