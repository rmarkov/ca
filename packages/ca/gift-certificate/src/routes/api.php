<?php

use CA\GiftCertificate\Http\Controllers\V1\GiftCertificateController as GiftCertificateControllerV1;
use CA\GiftCertificate\Http\Controllers\V1\GiftCertificateTypeController as GiftCertificateTypeControllerV1;
use CA\GiftCertificate\Http\Controllers\V2\GiftCertificateTypeController as GiftCertificateTypeControllerV2;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'api/v1/'], function () {
    Route::resource('gift-certificates', GiftCertificateControllerV1::class);
    Route::get('gift-certificates/check-card-balance', GiftCertificateControllerV1::class . '@checkGiftCardBalance');
    Route::resource('gift-certificate-types', GiftCertificateTypeControllerV1::class);
});

Route::group(['prefix' => 'api/v2/'], function () {
    Route::resource('gift-certificates', GiftCertificateControllerV1::class);
    Route::get('gift-certificates/check-card-balance', GiftCertificateControllerV1::class . '@checkGiftCardBalance');
    Route::resource('gift-certificate-types', GiftCertificateTypeControllerV2::class);
});
