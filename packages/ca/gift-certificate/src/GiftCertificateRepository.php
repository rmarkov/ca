<?php

namespace CA\GiftCertificate;

class GiftCertificateRepository
{
    /**
     * @var GiftCertificate
     */
    protected $giftCertificate;

    /**
     * GiftCertificateRepository constructor.
     * @param GiftCertificate $giftCertificate
     */
    public function __construct(GiftCertificate $giftCertificate)
    {
        $this->giftCertificate = $giftCertificate;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findOrFail($id)
    {
        return $this->giftCertificate->findOrFail($id);
    }

    public function all()
    {
        return $this->giftCertificate->all();
    }

    public function checkGiftCardBalance($code)
    {
        return $this->giftCertificate
            ->where('code', $code)
            ->firstOrFail();
    }
}
