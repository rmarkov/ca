<?php

namespace CA\GiftCertificate;

class GiftCertificateTypeRepository
{
    /**
     * @var GiftCertificateType
     */
    protected $giftCertificateType;

    /**
     * GiftCertificateTypeRepository constructor.
     * @param GiftCertificateType $giftCertificateType
     */
    public function __construct(GiftCertificateType $giftCertificateType)
    {
        $this->giftCertificateType = $giftCertificateType;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findOrFail($id)
    {
        return $this->giftCertificateType->findOrFail($id);
    }

    public function all()
    {
        return $this->giftCertificateType->all();
    }
}
