<?php

namespace CA\GiftCertificate;

use Illuminate\Database\Eloquent\Model;

class GiftCertificateType extends Model
{
    /**
     * @inheritdoc
     */
    protected $table = 'gift_certificate_type';

    public function certificates()
    {
        return $this->hasMany(GiftCertificate::class, 'certificate_type_id', 'id');
    }
}
