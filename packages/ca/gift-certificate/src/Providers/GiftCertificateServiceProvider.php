<?php

namespace CA\GiftCertificate\Providers;

use Illuminate\Support\ServiceProvider;

class GiftCertificateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/../routes/web.php';
        include __DIR__.'/../routes/api.php';
    }
}
