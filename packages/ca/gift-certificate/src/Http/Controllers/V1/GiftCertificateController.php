<?php

namespace CA\GiftCertificate\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use CA\GiftCertificate\GiftCertificateRepository;
use Illuminate\Http\Request;

class GiftCertificateController extends Controller
{
    /**
     * @var GiftCertificateRepository
     */
    protected $giftCertificateRepository;

    /**
     * @param GiftCertificateRepository $giftCertificateRepository
     */
    public function __construct(GiftCertificateRepository $giftCertificateRepository)
    {
        $this->giftCertificateRepository = $giftCertificateRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->giftCertificateRepository->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->giftCertificateRepository->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function checkGiftCardBalance(Request $request)
    {
        return $this->giftCertificateRepository->checkGiftCardBalance($request->code);
    }
}
