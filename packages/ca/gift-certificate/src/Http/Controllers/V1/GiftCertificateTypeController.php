<?php

namespace CA\GiftCertificate\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use CA\GiftCertificate\GiftCertificateTypeRepository;
use Illuminate\Http\Request;

class GiftCertificateTypeController extends Controller
{
    /**
     * @var GiftCertificateTypeRepository
     */
    protected $giftCertificateTypeRepository;

    /**
     * GiftCertificateTypeController constructor.
     * @param GiftCertificateTypeRepository $giftCertificateTypeRepository
     */
    public function __construct(GiftCertificateTypeRepository $giftCertificateTypeRepository)
    {
        $this->giftCertificateTypeRepository = $giftCertificateTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->giftCertificateTypeRepository->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->giftCertificateTypeRepository->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
