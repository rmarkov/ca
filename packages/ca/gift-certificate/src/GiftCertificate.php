<?php

namespace CA\GiftCertificate;

use Illuminate\Database\Eloquent\Model;

class GiftCertificate extends Model
{
    /**
     * @inheritdoc
     */
    protected $table = 'gift_certificate';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'code',
    ];

    public function certificateTypes()
    {
        return $this->hasOne(GiftCertificateType::class, 'id', 'certificate_type_id');
    }
}
